﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kunekune
{
    public partial class ProgressDiag : Form
    {
        /// <summary>
        /// メッセージ本体。コンストラクタで呼び出し元から受け取る。
        /// </summary>
        private string labeltext = "sample";

        /// <summary>
        /// 進捗進行率。コンストラクタで引数受け取りする。
        /// </summary>
        private int progressvalue = 10;

        /// <summary>
        /// ███するステータス。
        /// </summary>
        private int status = 0;

        // メッセージ
        private string strerror = "エラー：";
        private string strcancel = "キャンセルされました。";
        private string strcomplete = "完了しました。";
        private string strpercent = "%";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="labeltext">表示するメッセージ</param>
        /// <param name="progressvalue">プログレスバーの進行速度。</param>
        /// <param name="status">███</param>
        public ProgressDiag(string labeltext, int progressvalue, int status)
        {
            InitializeComponent();
            this.labeltext = labeltext;
            this.progressvalue = progressvalue;
            this.status = status;
        }

        /// <summary>
        /// 画面ロード時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressDiag_Load(object sender, EventArgs e)
        {
            // アイコンの描画
            Bitmap canvas = new Bitmap(pbxIcon.Width, pbxIcon.Height);
            Graphics g = Graphics.FromImage(canvas);
            g.DrawIcon(SystemIcons.Information, 0, 0);
            g.Dispose();
            pbxIcon.Image = canvas;

            // メッセージの表示
            label1.Text = labeltext;

            // ボタンの無効化
            btnClose.Enabled = false;

            // コントロールの初期化
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            progressBar1.Value = 0;
            txtPercent.Text = "0%";

            // statusに基づく言語の変換
            goOtherSide();

            // backgroundworker処理
            bgwProgress.WorkerReportsProgress = true;
            bgwProgress.WorkerSupportsCancellation = true;
            bgwProgress.RunWorkerAsync(progressvalue);
        }

        /// <summary>
        /// クローズボタン押下時処理
        ///   画面を閉じる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            // プログレスバーダイアログを閉じる。
            Close();
        }

        /// <summary>
        /// プログレスバー処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgWorker = (BackgroundWorker)sender;

            int progressvalue = (int)e.Argument;

            for(int i=1; i<=100; i=i+progressvalue)
            {
                if (bgWorker.CancellationPending)
                {
                    // キャンセル処理
                    e.Cancel = true;
                    return;
                }

                // スリープ
                System.Threading.Thread.Sleep(100);

                // 更新イベント呼び出し
                bgWorker.ReportProgress(i);
            }

            bgWorker.ReportProgress(100);
            e.Result = 100;
        }

        /// <summary>
        /// プログレスバー表示更新処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // プログレスバーの更新
            progressBar1.Value = e.ProgressPercentage;
            // テキストの更新
            txtPercent.Text = e.ProgressPercentage.ToString() + strpercent; 
        }

        /// <summary>
        /// 処理終了時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                // エラー時
                label1.Text = label1.Text + "\n..." + strerror + e.Error.Message;
            }
            else if (e.Cancelled)
            {
                // キャンセル時
                label1.Text = label1.Text + "\n..." + strcancel;
            }
            else
            {
                // 正常時
                label1.Text = label1.Text + "\n..." + strcomplete;
            }

            // ボタン有効無効
            btnCancel.Enabled = false;
            btnClose.Enabled = true;
        }

        /// <summary>
        /// キャンセルボタン
        ///   プログレスバーの処理をキャンセルして、クローズボタンを有効化する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            bgwProgress.CancelAsync();
        }

        /// <summary>
        /// すべてのコントロールの文字列が列挙されるには変換の検索してから
        /// </summary>
        private void goOtherSide()
        {
            if (status != 0)
            {
                // すべてのコントロールに列挙がアクセスします。
                List<Control> controls = getAllControls<Control>(this);
                foreach (Control c in controls)
                {
                    if (c.Text != null)
                    {
                        // 変換の文字列
                        c.Text = convert(c.Text, status);
                    }
                }

                // メッセージの変換
                strerror = convert(strerror, status);
                strcancel = convert(strcancel, status);
                strcomplete = convert(strcomplete, status);
                strpercent = convert(strpercent, status);
            }
        }

        /// <summary>
        /// 文字列の変換
        /// </summary>
        /// <param name="text">渡されるべきテキスト</param>
        /// <param name="status">変換変数</param>
        /// <returns></returns>
        private string convert(string text, int status)
        {
            // 文字列のバイト列への変換
            byte[] data = Encoding.UTF8.GetBytes(text.ToCharArray());
            // バイト列の編集
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)((data[i] + status) % 255);
            }
            // バイト列の文字列への変換
            string convtext = Encoding.GetEncoding("Shift-JIS").GetString(data, 0, data.Length);
            return convtext;
        }

        /// <summary>
        /// すべてのコントロールを取得する
        /// </summary>
        /// <typeparam name="T">対象の型</typeparam>
        /// <param name="top">指定のコントロール</param>
        /// <returns>指定のコントロール上のすべてのTコントロールのインスタンス</returns>
        private List<T> getAllControls<T>(Control top) where T : Control
        {
            List<T> buf = new List<T>();
            foreach (Control c in top.Controls)
            {
                if (c is T)
                {
                    buf.Add((T)c);
                }
                buf.AddRange(getAllControls<T>(c));
            }
            buf.Add((T)top);
            return buf;
        }
    }
}

