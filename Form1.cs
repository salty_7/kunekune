﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace kunekune
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// 実行ステータス。0意外にしないでください。
        /// </summary>
        private int status = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuit_Click(object sender, EventArgs e)
        {
            // アプリケーションを終了する。
            Close();
        }

        /// <summary>
        /// 反撃する心の準備をしています。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHangeki_Click(object sender, EventArgs e)
        {
            string text = "反撃する心の準備をしています。";
            int value = 2;
            ProgressDiag pd = new ProgressDiag(text, value, status);
            pd.ShowDialog();
        }

        /// <summary>
        /// 外出する心の準備をしています。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGaisyutu_Click(object sender, EventArgs e)
        {
            string text = "外出する心の準備をしています。";
            int value = 2;
            ProgressDiag pd = new ProgressDiag(text, value, status);
            pd.ShowDialog();
        }

        /// <summary>
        /// ボタンクリックイベント。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConvert_Click(object sender, EventArgs e)
        {
            status = 2043;
            goOtherSide();
        }

        /// <summary>
        /// すべてのコントロールの文字列が列挙されるには変換の検索してから
        /// </summary>
        private void goOtherSide()
        {
            if (status != 0)
            {
                // すべてのコントロールに列挙がアクセスします。
                List<Control> controls = getAllControls<Control>(this);
                foreach (Control c in controls)
                {
                    if(c.Text != null)
                    {
                        // 変換の文字列
                        c.Text = convert(c.Text, status);
                    }
                }
            }
        }

        /// <summary>
        /// 文字列の変換
        /// </summary>
        /// <param name="text">渡されるべきテキスト</param>
        /// <param name="status">変換変数</param>
        /// <returns></returns>
        private string convert(string text, int status)
        {
            // 文字列のバイト列への変換
            byte[] data = Encoding.UTF8.GetBytes(text.ToCharArray());
            // バイト列の編集
            for(int i=0; i<data.Length; i++)
            {
                data[i] = (byte)((data[i] + status) % 255);
            }
            // バイト列の文字列への変換
            string convtext = Encoding.GetEncoding("Shift-JIS").GetString(data, 0, data.Length);
            return convtext;
        }

        /// <summary>
        /// すべてのコントロールを取得する
        /// </summary>
        /// <typeparam name="T">対象の型</typeparam>
        /// <param name="top">指定のコントロール</param>
        /// <returns>指定のコントロール上のすべてのTコントロールのインスタンス</returns>
        private List<T> getAllControls<T>(Control top) where T : Control
        {
            List<T> buf = new List<T>();
            foreach (Control c in top.Controls)
            {
                if(c is T)
                {
                    buf.Add((T)c);
                }
                buf.AddRange(getAllControls<T>(c));
            }
            buf.Add((T)top);
            return buf;
        }
    }
}
